﻿// This file is part of FilterIsolator.
// FilterIsolator is free software developed by LezSoft. While LezSoft holds all rights on the FilterIsolator brand and logo,
// you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
// Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
// 
// SPDX-FileCopyrightText:  © 2020-2022 Lezsoft <https://lezsoft.com>
// SPDX-License-Identifier: GPL-3.0-or-later

using Autodesk.Revit.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FilterIsolator
{
    class FilterOptions
    {
        #region Fields

        string name;
        bool isolate;
        bool hide;

        IsolatorForm isolatorForm = null;

        #endregion

        #region Constructor

        public FilterOptions(IsolatorForm form, string name, bool isolate = false, bool hide = false)
        {
            this.isolatorForm = form;

            this.name = name;
            this.isolate = isolate;
            this.hide = hide;
        }

        #endregion

        #region Proprieties

        /// <summary>
        /// Get the filter name
        /// </summary>
        public string Name
        {
            get { return name; }
        }

        /// <summary>
        /// Get and set Isolate checkbox
        /// </summary>
        public bool Isolate
        {
            get { return isolate; }
            set 
            {
                // before setting true this Isolate checkbox...
                if (value == true)
                {
                    // uncheck all Hide checkboxes
                    isolatorForm.HideNone_Btn_Click(null, null);
                    // change the ApplyMode
                    isolatorForm.ApplyMode = ApplyMode.Isolate;
                }
                isolate = value; 
            }
        }

        /// <summary>
        /// Get and set the Hide checkbox
        /// </summary>
        public bool Hide
        {
            get { return hide; }
            set 
            {
                // before setting true this Hide checkbox
                if (value == true)
                {
                    // uncheck all Isolate checkboxes
                    isolatorForm.IsolateNone_Btn_Click(null, null);
                    // change the ApplyMode
                    isolatorForm.ApplyMode = ApplyMode.Hide;
                }
                hide = value; 
            }
        }

        #endregion
    }
}
