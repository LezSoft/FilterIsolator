﻿namespace FilterIsolator
{
    partial class IsolatorForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IsolatorForm));
            this.filters_DGV = new System.Windows.Forms.DataGridView();
            this.HideAll_Btn = new System.Windows.Forms.Button();
            this.HideNone_Btn = new System.Windows.Forms.Button();
            this.IsolateAll_Btn = new System.Windows.Forms.Button();
            this.IsolateNone_Btn = new System.Windows.Forms.Button();
            this.Apply_Btn = new System.Windows.Forms.Button();
            this.Cancel_Btn = new System.Windows.Forms.Button();
            this.reset_Btn = new System.Windows.Forms.Button();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.isolateDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.hideDataGridViewCheckBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.filterOptionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.filters_DGV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.filterOptionsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // filters_DGV
            // 
            resources.ApplyResources(this.filters_DGV, "filters_DGV");
            this.filters_DGV.AutoGenerateColumns = false;
            this.filters_DGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.filters_DGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.filters_DGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nameDataGridViewTextBoxColumn,
            this.isolateDataGridViewCheckBoxColumn,
            this.hideDataGridViewCheckBoxColumn});
            this.filters_DGV.DataSource = this.filterOptionsBindingSource;
            this.filters_DGV.Name = "filters_DGV";
            this.filters_DGV.CellMouseUp += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.filters_DGV_OnCellMouseUp);
            // 
            // HideAll_Btn
            // 
            resources.ApplyResources(this.HideAll_Btn, "HideAll_Btn");
            this.HideAll_Btn.BackColor = System.Drawing.Color.Gainsboro;
            this.HideAll_Btn.FlatAppearance.BorderSize = 0;
            this.HideAll_Btn.Name = "HideAll_Btn";
            this.HideAll_Btn.UseVisualStyleBackColor = false;
            this.HideAll_Btn.Click += new System.EventHandler(this.HideAll_Btn_Click);
            // 
            // HideNone_Btn
            // 
            resources.ApplyResources(this.HideNone_Btn, "HideNone_Btn");
            this.HideNone_Btn.BackColor = System.Drawing.Color.Gainsboro;
            this.HideNone_Btn.FlatAppearance.BorderSize = 0;
            this.HideNone_Btn.Name = "HideNone_Btn";
            this.HideNone_Btn.UseVisualStyleBackColor = false;
            this.HideNone_Btn.Click += new System.EventHandler(this.HideNone_Btn_Click);
            // 
            // IsolateAll_Btn
            // 
            this.IsolateAll_Btn.BackColor = System.Drawing.Color.Gainsboro;
            this.IsolateAll_Btn.FlatAppearance.BorderSize = 0;
            resources.ApplyResources(this.IsolateAll_Btn, "IsolateAll_Btn");
            this.IsolateAll_Btn.Name = "IsolateAll_Btn";
            this.IsolateAll_Btn.UseVisualStyleBackColor = false;
            this.IsolateAll_Btn.Click += new System.EventHandler(this.IsolateAll_Btn_Click);
            // 
            // IsolateNone_Btn
            // 
            this.IsolateNone_Btn.BackColor = System.Drawing.Color.Gainsboro;
            this.IsolateNone_Btn.FlatAppearance.BorderSize = 0;
            resources.ApplyResources(this.IsolateNone_Btn, "IsolateNone_Btn");
            this.IsolateNone_Btn.Name = "IsolateNone_Btn";
            this.IsolateNone_Btn.UseVisualStyleBackColor = false;
            this.IsolateNone_Btn.Click += new System.EventHandler(this.IsolateNone_Btn_Click);
            // 
            // Apply_Btn
            // 
            resources.ApplyResources(this.Apply_Btn, "Apply_Btn");
            this.Apply_Btn.BackColor = System.Drawing.Color.Gainsboro;
            this.Apply_Btn.FlatAppearance.BorderSize = 0;
            this.Apply_Btn.Name = "Apply_Btn";
            this.Apply_Btn.UseVisualStyleBackColor = false;
            this.Apply_Btn.Click += new System.EventHandler(this.Apply_Btn_Click);
            // 
            // Cancel_Btn
            // 
            resources.ApplyResources(this.Cancel_Btn, "Cancel_Btn");
            this.Cancel_Btn.BackColor = System.Drawing.Color.Gainsboro;
            this.Cancel_Btn.FlatAppearance.BorderSize = 0;
            this.Cancel_Btn.Name = "Cancel_Btn";
            this.Cancel_Btn.UseVisualStyleBackColor = false;
            this.Cancel_Btn.Click += new System.EventHandler(this.Cancel_Btn_Click);
            // 
            // reset_Btn
            // 
            resources.ApplyResources(this.reset_Btn, "reset_Btn");
            this.reset_Btn.BackColor = System.Drawing.Color.Gainsboro;
            this.reset_Btn.FlatAppearance.BorderSize = 0;
            this.reset_Btn.Name = "reset_Btn";
            this.reset_Btn.UseVisualStyleBackColor = false;
            this.reset_Btn.Click += new System.EventHandler(this.reset_Btn_Click);
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            resources.ApplyResources(this.nameDataGridViewTextBoxColumn, "nameDataGridViewTextBoxColumn");
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            this.nameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // isolateDataGridViewCheckBoxColumn
            // 
            this.isolateDataGridViewCheckBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.isolateDataGridViewCheckBoxColumn.DataPropertyName = "Isolate";
            resources.ApplyResources(this.isolateDataGridViewCheckBoxColumn, "isolateDataGridViewCheckBoxColumn");
            this.isolateDataGridViewCheckBoxColumn.Name = "isolateDataGridViewCheckBoxColumn";
            // 
            // hideDataGridViewCheckBoxColumn
            // 
            this.hideDataGridViewCheckBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.hideDataGridViewCheckBoxColumn.DataPropertyName = "Hide";
            resources.ApplyResources(this.hideDataGridViewCheckBoxColumn, "hideDataGridViewCheckBoxColumn");
            this.hideDataGridViewCheckBoxColumn.Name = "hideDataGridViewCheckBoxColumn";
            // 
            // filterOptionsBindingSource
            // 
            this.filterOptionsBindingSource.DataSource = typeof(FilterIsolator.FilterOptions);
            // 
            // IsolatorForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.reset_Btn);
            this.Controls.Add(this.Cancel_Btn);
            this.Controls.Add(this.Apply_Btn);
            this.Controls.Add(this.IsolateNone_Btn);
            this.Controls.Add(this.IsolateAll_Btn);
            this.Controls.Add(this.HideNone_Btn);
            this.Controls.Add(this.HideAll_Btn);
            this.Controls.Add(this.filters_DGV);
            this.Name = "IsolatorForm";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.IsolatorForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.filters_DGV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.filterOptionsBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView filters_DGV;
        private System.Windows.Forms.BindingSource filterOptionsBindingSource;
        private System.Windows.Forms.Button HideAll_Btn;
        private System.Windows.Forms.Button HideNone_Btn;
        private System.Windows.Forms.Button IsolateAll_Btn;
        private System.Windows.Forms.Button IsolateNone_Btn;
        private System.Windows.Forms.Button Apply_Btn;
        private System.Windows.Forms.Button Cancel_Btn;
        private System.Windows.Forms.Button reset_Btn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn isolateDataGridViewCheckBoxColumn;
        private System.Windows.Forms.DataGridViewCheckBoxColumn hideDataGridViewCheckBoxColumn;
    }
}