﻿// This file is part of FilterIsolator.
// FilterIsolator is free software developed by LezSoft. While LezSoft holds all rights on the FilterIsolator brand and logo,
// you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
// Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
// 
// SPDX-FileCopyrightText:  © 2020-2022 Lezsoft <https://lezsoft.com>
// SPDX-License-Identifier: GPL-3.0-or-later

using Autodesk.Revit.UI;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace FilterIsolator
{
    public class App : IExternalApplication
    {
        const string MyRibbonTab = "LezTools";
        const string MyRibbonPanel = "Filter Isolator";

        const string HelpPageUrl = "https://lezsoft.com/filter-isolator/";

        public Result OnStartup(UIControlledApplication application)
        {
            // create the Ribbon Tab (if it doesn't exist)
            try
            {
                application.CreateRibbonTab(MyRibbonTab);
            }
            catch (Exception) { } // if there is an exception, the tab already exists

            //get the Panel
            RibbonPanel panel = null;
            List<RibbonPanel> panels = application.GetRibbonPanels(MyRibbonTab);
            foreach (RibbonPanel p in panels)
            {
                if (p.Name == MyRibbonPanel)
                {
                    panel = p;
                    break;
                }
            }

            // create the pannel if it doesn't already exist
            if (panel == null)
            {
                panel = application.CreateRibbonPanel(MyRibbonTab, MyRibbonPanel);
            }

            // get the Open Isolator Button icons
            Image isolatorBtn_img16 = Properties.Resources.IsolateFilter16;
            Image isolatorBtn_img32 = Properties.Resources.IsolateFilter32;

            // create the Open Isolator Button data
            PushButtonData isolatorBtnData = new PushButtonData(
                "IsolatorButton",
                "Isolator",
                Assembly.GetExecutingAssembly().Location,
                "FilterIsolator.OpenIsolatorCommand")
            {
                ToolTip = "Open the Isolator Window",
                Image = GetImageSource(isolatorBtn_img16),
                LargeImage = GetImageSource(isolatorBtn_img32)
            };

            // add the button to the panel
            PushButton isolatorBtn = panel.AddItem(isolatorBtnData) as PushButton;

            // create the F1-Help reference and add it to the button
            ContextualHelp contextualHelp = new ContextualHelp(ContextualHelpType.Url, HelpPageUrl);
            isolatorBtn.SetContextualHelp(contextualHelp);

            return Result.Succeeded;
        }

        public Result OnShutdown(UIControlledApplication application)
        {
            return Result.Succeeded;
        }

        /// <summary>
        /// Get the BitmapSource of an image (code pasted from https://youtu.be/lYjHDkpbsas?t=747)
        /// </summary>
        /// <param name="img">The Image</param>
        /// <returns>The BitmapSource of the given image</returns>
        BitmapSource GetImageSource(Image img)
        {
            BitmapImage bmp = new BitmapImage();

            using (MemoryStream ms = new MemoryStream())
            {
                img.Save(ms, ImageFormat.Png);
                ms.Position = 0;

                bmp.BeginInit();

                bmp.CacheOption = BitmapCacheOption.OnLoad;
                bmp.UriSource = null;
                bmp.StreamSource = ms;

                bmp.EndInit();
            }

            return bmp;
        }
    }
}
