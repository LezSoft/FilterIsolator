﻿// This file is part of FilterIsolator.
// FilterIsolator is free software developed by LezSoft. While LezSoft holds all rights on the FilterIsolator brand and logo,
// you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
// Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
// 
// SPDX-FileCopyrightText:  © 2020-2022 Lezsoft <https://lezsoft.com>
// SPDX-License-Identifier: GPL-3.0-or-later

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FilterIsolator
{
    public enum ApplyMode
    {
        Null,
        Isolate,
        Hide
    }
}
