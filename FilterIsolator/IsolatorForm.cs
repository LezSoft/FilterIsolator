﻿// This file is part of FilterIsolator.
// FilterIsolator is free software developed by LezSoft. While LezSoft holds all rights on the FilterIsolator brand and logo,
// you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
// Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.
// 
// SPDX-FileCopyrightText:  © 2020-2022 Lezsoft <https://lezsoft.com>
// SPDX-License-Identifier: GPL-3.0-or-later

using Autodesk.Revit.DB;
using Autodesk.Revit.DB.Events;
using Autodesk.Revit.UI;
using Autodesk.Revit.UI.Selection;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Autodesk.Revit.ApplicationServices;

namespace FilterIsolator
{
    public partial class IsolatorForm : System.Windows.Forms.Form
    {
        #region Fields

        UIDocument uiDoc = null;
        Document doc;

        Dictionary<string, FilterElement> filterElements = new Dictionary<string, FilterElement>();

        ApplyMode applyMode;

        // [DEBUG STUFF]
        // a list which keeps all elements which would have thrown a NullArgumentException when attempted to get their category
        //List<Element> problematicElements = new List<Element>();

        #endregion

        #region Proprieties

        /// <summary>
        /// Get or set the applyMode field
        /// </summary>
        public ApplyMode ApplyMode
        {
            get { return applyMode; }
            set { applyMode = value; }
        }

        # endregion

        #region Methods

        public IsolatorForm(UIDocument uIDocument)
        {
            uiDoc = uIDocument;
            InitializeComponent();
        }

        private void IsolatorForm_Load(object sender, EventArgs e)
        {
            // get the current document
            doc = uiDoc.Document;

            // add a listener for the document close event
            doc.Application.DocumentClosed += Application_DocumentClosed;

            // collect all document's filters
            FilteredElementCollector collector = new FilteredElementCollector(doc).OfClass(typeof(FilterElement));

            // move collected filters to a list and sort it by filter Name
            List<FilterElement> collectedFilters = new List<FilterElement>();
            foreach (FilterElement filter in collector)
            {
                collectedFilters.Add(filter);
            }
            collectedFilters.Sort(FilterElementsComparison);

            // add filters in the list to the FilterElements Dictionary and to the FilterOptions Binding Source
            foreach (FilterElement filter in collectedFilters)
            {
                // add the FilterElement to the local dictionary
                filterElements.Add(filter.Name, filter);

                // if there are existing data for this filter stored in the DataKeeper...
                if (DataKeeper.Filters.ContainsKey(filter.Name))
                {
                    // get checkboxes from DataKeeper
                    bool isolate = DataKeeper.Filters[filter.Name].Isolate;
                    bool hide = DataKeeper.Filters[filter.Name].Hide;

                    // add a new FilterOptions wit those data to the FilterOptionsBindingSource
                    filterOptionsBindingSource.Add(new FilterOptions(this, filter.Name, isolate, hide));

                    // update the DataKeeper (replace the old FilterOption with a new one with the same data but the current IsolatorForm)
                    //DataKeeper.Filters[filter.Name] = new FilterOptions(this, filter.Name, isolate, hide);
                }
                else
                {
                    // else add a new FilterOptions to the FilterOptionsBindingSource
                    filterOptionsBindingSource.Add(new FilterOptions(this, filter.Name));
                    // and update the DataKeeper
                    //DataKeeper.Filters.Add(filter.Name, new FilterOptions(this, filter.Name));
                }
            }

            // get the previous ApplyMode (if there is one)
            if (DataKeeper.ApplyMode != ApplyMode.Null)
            {
                applyMode = DataKeeper.ApplyMode;
            }
        }

        /// <summary>
        /// Handle the DocumentClosed Event (invoked by the application)
        /// </summary>
        private void Application_DocumentClosed(object sender, DocumentClosedEventArgs e)
        {
            // clear the DataKeeper
            DataKeeper.Filters = new Dictionary<string, FilterOptions>();
            DataKeeper.ApplyMode = ApplyMode.Null;
            
            // close the form
            this.Close();
        }

        /// <summary>
        /// Handle IsolateAll Button click
        /// </summary>
        private void IsolateAll_Btn_Click(object sender, EventArgs e)
        {
            // check all Isolate checkboxes and refresh the DGV
            foreach (FilterOptions filter in filterOptionsBindingSource)
            {
                filter.Isolate = true;
            }
            filters_DGV.Refresh();
        }

        /// <summary>
        /// Handle HideAll Button click
        /// </summary>
        private void HideAll_Btn_Click(object sender, EventArgs e)
        {
            // check all Hide checkboxes and refresh the DGV
            foreach (FilterOptions filter in filterOptionsBindingSource)
            {
                filter.Hide = true;
            }
            filters_DGV.Refresh();
        }

        /// <summary>
        /// Handle IsolateNone Button click (public becouse is used also in the FilterOptions class)
        /// </summary>
        public void IsolateNone_Btn_Click(object sender, EventArgs e)
        {
            // uncheck all Isolate checkboxes and refresh the DGV
            foreach (FilterOptions filter in filterOptionsBindingSource)
            {
                filter.Isolate = false;
            }
            filters_DGV.Refresh();
        }

        /// <summary>
        /// Handle HideNone Button click
        /// </summary>
        public void HideNone_Btn_Click(object sender, EventArgs e)
        {
            // uncheck all Hide checkboxes and refresh the DGV
            foreach (FilterOptions filter in filterOptionsBindingSource)
            {
                filter.Hide = false;
            }
            filters_DGV.Refresh();
        }

        /// <summary>
        /// Handle Reset Button click
        /// </summary>
        private void reset_Btn_Click(object sender, EventArgs e)
        {
            // store the current view
            Autodesk.Revit.DB.View view = doc.ActiveView;

            // clear other temporary views
            view.DisableTemporaryViewMode(TemporaryViewMode.TemporaryHideIsolate);
            uiDoc.RefreshActiveView();

            // clear checkboxes
            foreach (FilterOptions filter in filterOptionsBindingSource)
            {
                filter.Isolate = false;
                filter.Hide = false;
            }
            filters_DGV.Refresh();
            // update the DataKeeper
            DataKeeper.Filters = new Dictionary<string, FilterOptions>();
            foreach (FilterOptions filter in DataKeeper.Filters.Values)
            {
                DataKeeper.Filters.Add(filter.Name, new FilterOptions(this, filter.Name));
            }
        }

        /// <summary>
        /// Handle Cancel Button click
        /// </summary>
        private void Cancel_Btn_Click(object sender, EventArgs e)
        {
            // close the form
            this.Close();
        }

        /// <summary>
        /// Handle Apply Button click
        /// </summary>
        private void Apply_Btn_Click(object sender, EventArgs e)
        {
            // Update the DataKeeper
            DataKeeper.Filters = new Dictionary<string, FilterOptions>();
            foreach (FilterOptions filter in filterOptionsBindingSource)
            {
                
                DataKeeper.Filters.Add(filter.Name, new FilterOptions(this, filter.Name, filter.Isolate, filter.Hide));
            }
            DataKeeper.ApplyMode = applyMode;

            // store the current view
            Autodesk.Revit.DB.View view = doc.ActiveView;

            // clear other temporary views
            view.DisableTemporaryViewMode(TemporaryViewMode.TemporaryHideIsolate);

            // [DEBUG STUFF]
            //problematicElements.Clear();

            if (applyMode == ApplyMode.Isolate)
            {
                // list of ids of the elements to isolate
                List<ElementId> elementIds = new List<ElementId>();

                // collect elements of each filter which has Isolate checkbox checked
                foreach (FilterOptions filter in filterOptionsBindingSource)
                {
                    if (filter.Isolate == true)
                    {
                        foreach (ElementId id in GetFilterElementsIds(filter))
                        {
                            elementIds.Add(id);
                        }
                    }
                }

                // isolate collected elements
                view.IsolateElementsTemporary(elementIds);
                uiDoc.RefreshActiveView();
            }
            else if (applyMode == ApplyMode.Hide)
            {
                // list of ids of the elements to isolate
                List<ElementId> elementIds = new List<ElementId>();

                // collect elements of each filter which has Hide checkbox checked
                foreach (FilterOptions filter in filterOptionsBindingSource)
                {
                    if (filter.Hide == true)
                    {
                        foreach (ElementId id in GetFilterElementsIds(filter))
                        {
                            elementIds.Add(id);
                        }
                    }
                }

                // hide collected elements
                view.HideElementsTemporary(elementIds);
                uiDoc.RefreshActiveView();
            }
        }

        /// <summary>
        /// Get ids of elements connected with a filter 
        /// </summary>
        /// <param name="filter">the FilterOption class of the filter</param>
        /// <returns>a list of connected elements' ids</returns>
        List<ElementId> GetFilterElementsIds(FilterOptions filter)
        {
            //create the list to return
            List<ElementId> elementIds = new List<ElementId>();
            
            // get the respective filter element
            FilterElement filterElement = filterElements[filter.Name];

            // check if it's a selection filter or a parameter filter and...
            if (filterElement is SelectionFilterElement)
            {
                // cast it into a new SelectionFilter Element
                SelectionFilterElement selectionFilter = filterElement as SelectionFilterElement;

                // add the ids of the elements connected to that filter to the list
                foreach (ElementId id in selectionFilter.GetElementIds())
                {
                    elementIds.Add(id);
                }
            }
            else
            {
                // cast it into a new ParameterFilter Element
                ParameterFilterElement parameterFilter = filterElement as ParameterFilterElement;

                // get the ids of the categories accepted by the filter
                List<ElementId> filterCategoriesIds = parameterFilter.GetCategories() as List<ElementId>;

                // get an ElementFilter (different from Filter Element) with the conditions of this ParameterFilter Element
                ElementFilter elementFilter = parameterFilter.GetElementFilter();

                // if the filter has no parameters (and so the new-created ElementFilter is null)
                if (elementFilter == null)
                {
                    // add all elements of the filter categories to the list
                    foreach (ElementId category in filterCategoriesIds)
                    {
                        FilteredElementCollector collector = new FilteredElementCollector(doc).OfCategoryId(category);
                        foreach (Element element in collector)
                        {
                            elementIds.Add(element.Id);
                        }
                    }

                }
                else
                {
                    // get the elements in the document which passes the new-created ElementFilter and add them to the list
                    List<Element> collector = new FilteredElementCollector(doc).WherePasses(elementFilter).ToElements() as List<Element>;

                    foreach (Element element in collector)
                    {
                        if (element.Category != null)
                        {
                            // check if the element is of a category accepted by the filter [USED TO SOLVE THE BUG]
                            if (filterCategoriesIds.Contains(element.Category.Id))
                            {
                                elementIds.Add(element.Id);
                            }
                        }
                        // [DEBUG STUFF]
                        //else
                        //{
                        //    // store the element which would have thrown a NullArgumentException in a list (if it's not already stored)
                        //    if (!problematicElements.Contains(element))
                        //    {
                        //        problematicElements.Add(element);
                        //    }
                        //}
                    }
                }
            }

            return elementIds;
        }

        /// <summary>
        /// A Comparasion for FilterElements
        /// </summary>
        /// <param name="filter1">first filter to compare</param>
        /// <param name="filter2">second filter to compare</param>
        /// <returns>1 if filter1 is greater than filter2, -1 if it's smaller and 0 if it's equal</returns>
        int FilterElementsComparison(FilterElement filter1, FilterElement filter2)
        {
            if (filter1.Name.CompareTo(filter2.Name) > 0)
            {
                return 1;
            }
            else if (filter1.Name.CompareTo(filter2.Name) < 0)
            {
                return -1;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Handle when a mouse button is raised on a cell
        /// </summary>
        private void filters_DGV_OnCellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        { 
            // if the current cell is one in the "Isolate" column or in the "Hide" column (and it isn't the Header cell)
            if (e.RowIndex != -1 &&
                (e.ColumnIndex == 1 || e.ColumnIndex == 2))
            {
                // commit changes and end edit-mode (so that isn't necessary click somewhere else to save)
                filters_DGV.EndEdit();
            }
        }

        #endregion
    }
}
