<!--
  This file is part of FilterIsolator.
  FilterIsolator is free software developed by LezSoft. While LezSoft holds all rights on the FilterIsolator brand and logo,
  you can redistribute and/or modify this code under the terms of the GNU General Public License version 3.
  Please see the 'Copyright and License' section of the 'README.md' file in this repository to learn more.

  SPDX-FileCopyrightText:  © 2020-2022 Lezsoft <https://lezsoft.com>
  SPDX-License-Identifier: GPL-3.0-or-later
-->

# [Filter Isolator](https://lezsoft.com/filter-isolator)

### ReadMe Navigation:
1. [What is *Filter Isolator*](#what-is-filter-isolator)
2. [Features:](#features)
3. [Contributing Guidelines](#contributing-guidelines)
4. [Copyright and License](#copyright-and-license)

---
### What is *Filter Isolator*
*Filter Isolator* is a plugin for Autodesk Revit© that lets you easily Isolate or Hide all elements in one or multiple filters with few clicks.
It's [available on the Autodesk App Store©](https://apps.autodesk.com/RVT/it/Detail/Index?id=3543646314782540604&os=Win64&appLang=en) and it's compatible with all Revit© versions after the 2020 one.

---
### Features:
The plugin will add an `Isolator` button to our custom ribbon tab (`LezTools`)

Clicking this button will open a form where you will find the following options:

* `Isolate All` - check all checkboxes in the *Isolate* column
* `Isolate None` - uncheck all checkboxes in the *Isolate* column
* `Hide All` - check all checkboxes in the *Hide* column
* `Hide None` - uncheck all checkboxes in the *Hide* column
* `Reset Temporary View` - close the temporary view and show again all elements in the active view
* `Cancel` - close the *Isolator* form
* `Apply` - isolate or hide all elements of selected filters in the active view

---
### Contributing Guidelines
We appreciate all contributions and Pull Requests since we think that cooperation between developers and users is a core part of open-source. However, to ensure that the new code works flawlessly with the old one and share its "style", all Pull Requests will be reviewed by a LezSoft developer before being merged.

If you have any non-code suggestion please open an Issue here on Codeberg so that it will be discussed togheter with all the community.

If you have any question feel free to reach us [by email](mailto:info@lezsoft.com)

---
### Copyright and License
This project has been developed by [LezSoft](https://lezsoft.com). All the rights on the *Filter Isolator* brand (with its name and logo) are reserved.

*Filter Isolator* is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

*Filter Isolator* is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.